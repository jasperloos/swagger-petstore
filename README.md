# Swagger Petstore Instructions

## Installation
1. [Install Java version 8](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html)
2. [Install Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
3. [Install IntelliJ](https://www.jetbrains.com/help/idea/installation-guide.html)
4. [Install Maven](https://maven.apache.org/install.html)
5. [Configure Cucumber in IntelliJ](https://www.jetbrains.com/help/idea/enabling-cucumber-support-in-project.html)
6. Clone the Git repository from GitLab link https://gitlab.com/jasperloos/swagger-petstore.git to your local machine
7. Start a new IntelliJ project based on the cloned Git project
8. In IntelliJ, open Maven, select clean and install and click the play-icon with "Run Maven Build"
9. Verify that BUILD SUCCES is returned

## Running tests
If you want to run a specific Feature file, right-click the feature and click “Run”.

If you want to run a specific Scenario, right-click the scenario and click “Run”.

## Creating tests
If you want to create a new test, first pull the latest code from the master branch, and then create a new branch.
Write a new feature according to the examples provided in this document.
Create a pull request, have it reviewed by a colleague, and if everything is okay, merge.