@nightly @pets
Feature: Everything about pets

  As a pet store owner
  I want to be able to add data about pets in my store
  So that I have the correct data about pets

  Background: Import a pet
    Given pet 'Lassie' is available in the store

  Scenario: Retrieve pet by ID
    When I retrieve the pet by ID
    Then pet 'Lassie' is retrieved successfully

  Scenario: Retrieve pet with invalid ID
    When I retrieve a pet with an invalid ID
    Then there is no pet retrieved