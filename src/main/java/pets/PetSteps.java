package pets;

import static org.hamcrest.Matchers.equalTo;

import static net.serenitybdd.rest.SerenityRest.given;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;

public class PetSteps
{
    private static final String BASE_URL = "https://petstore.swagger.io/v2/";
    private static final String PET_ENDPOINT = "pet/";
    private Long petId;
    private Response response;

    @Given("^pet '(.+)' is available in the store$")
    public void importPetInStore(String petName)
    {
        //Does a POST-call to store the pet and retrieves the petId for usage in other methods.
        petId = given()
            .contentType("application/json")
            .body("{\"name\": \"" + petName + "\"}")
            .when()
            .post(BASE_URL + PET_ENDPOINT)
            .then()
            .extract()
            .path("id");
    }

    @When("^I retrieve the pet by ID$")
    public void retrieveThePetById()
    {
        //Retrieves the pet by ID and retrieves the responseBody for usage in the validation step.
        response = given()
            .when()
            .get(BASE_URL + PET_ENDPOINT + petId);
    }

    @Then("^pet '(.+)' is retrieved successfully$")
    public void verifyPetIsRetrievedSuccessfully(String petName)
    {
        //Verifies the responseBody and name in the JSON-response.
        response
            .then()
            .statusCode(200)
            .and().body("name", equalTo(petName));
    }

    @When("^I retrieve a pet with an invalid ID$")
    public void retrieveAPetWithAnInvalidId()
    {
        //Uses an invalid ID to get a pet by ID and retrieves the responseBody for usage in the validation step.
        long invalidId = petId + 10000000;
        response = given()
            .when()
            .get(BASE_URL + PET_ENDPOINT + invalidId);
    }

    @Then("^there is no pet retrieved$")
    public void verifyThereIsNoPetRetrieved()
    {
        //Verifies that there is no pet found.
        response
            .then()
            .statusCode(404)
            .and().body("message", equalTo("Pet not found"));
    }
}
